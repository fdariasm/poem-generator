package velocity

/**
 * Created by fdariasm.
 */

case class Rule(name: String)

sealed trait Definition
case class Word(name: String) extends Definition
case class Keyword(name: String) extends Definition
case class Reference(rule: Rule) extends Definition
sealed trait Grouping extends Definition
case class WordGrouping(words: List[Word]) extends Grouping
case class ComposedWord(words: WordGrouping, or: DefOrGrouping) extends Definition
case class DefOrGrouping(definitions: List[Definition]) extends Grouping
case class DefListGrouping(definitions: List[Definition]) extends Grouping

import fastparse.all._

trait PoemGeneratorParser {

  val rule = P(CharIn('A' to 'Z').rep.! ~ ": ").map(Rule(_))
  val word = P(CharIn('a' to 'z').rep.!).map(Word(_))
  val keyword = P("$LINEBREAK".! | "$END".!).map(Keyword(_))
  val reference = P("<" ~ CharIn('A' to 'Z').rep.! ~ ">").map(x => Reference(Rule(x)))

  val words = word.rep(sep = "|").map(x => WordGrouping(x.toList))

  val orDef: Parser[Definition] = reference | keyword
  val or = orDef.rep(min=1, sep = "|").map(x => DefOrGrouping(x.toList))

  val composedWord = P(words ~ " " ~ or).map {
    case (x, y) => ComposedWord(x, y)
  }

  val concat_def = P(or.rep(min=1,sep = " ")).map(x => DefListGrouping(x.toList))

  val definition: Parser[(Rule, Definition)] = P(rule ~ (concat_def | composedWord))

  def process(input: String): (Rule, Definition) = {
    definition.parse(input) match {
      case Parsed.Success((r, d), i)    => (r, d)
      case Parsed.Failure(parser, a, b) => throw new RuntimeException(b.toString)
    }
  }
}

object WebFilterParsercombinator extends PoemGeneratorParser {
  def main_(args: Array[String]): Unit = {
    definition.parse("""POEM: <LINE> <LINE> <LINE> <LINE> <LINE>""") match {
      case Parsed.Success((r, d), i) =>
        println(r)
        println(d)
        println(s"Rest: ${i}")
      case Parsed.Failure(parser, a, b) =>
        println(b)
    }
  }

}