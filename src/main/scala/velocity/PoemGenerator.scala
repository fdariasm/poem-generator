package velocity

import scala.io.Source
import scala.io.BufferedSource

/**
 * Created by fdariasm.
 */

object PoemGenerator{
    
  def main(args: Array[String]): Unit = {
    
    val grammarFile = getGrammarFile(args)
    
    val debug = isDebug(args)
        
    val fromFile = grammarFile.getLines().toList
    
    val parser = new PoemGeneratorParser(){}
    val rules = fromFile.map(parser.process(_)).toMap
    
    val composer = new PoemGeneratorComposer(){
      val debugEnabled = debug 
    }
    
    if(debug)
      for((r, d) <- rules)println(r + ": " + d)
      
    println("**************************")
    println(composer.composePoem(rules))
  }
  
  def getGrammarFile(args: Array[String]): BufferedSource = {
    if(args.size == 0) Source.fromURL(getClass.getResource("/grammar.txt"))
    else Source.fromFile(args(0))
  }
  
  def isDebug(args: Array[String]): Boolean = {
    args.size > 1 && "d" == args(1)
  }

}