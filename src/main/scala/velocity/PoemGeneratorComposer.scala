package velocity

import scala.util.Random

/**
 * Created by fdariasm.
 */

sealed trait PGError
case class ParseError(description: String) extends PGError
case class CompositionError(rule: Rule, description: String) extends PGError

trait PoemGeneratorComposer {
  type Composition = Either[CompositionError, String]

  val newLine = System.getProperty("line.separator")

  def debugEnabled: Boolean

  def composePoem(rules: Map[Rule, Definition]): String = {
    compose(Rule("POEM"), rules)
  }

  def compose(rule: Rule, rules: Map[Rule, Definition]): String = {
    doInDebug(() => println(s"Composing for rule: $rule"))
    rules.get(rule) match {
      case Some(definition) => composeDefinition(definition, rules)
      case None             => throw new RuntimeException(s"${rule.name} not found") //Left(CompositionError(rule, "Rule not found"))
    }
  }

  def composeDefinition(definition: Definition, rules: Map[Rule, Definition]): String = {
    doInDebug(() => println(s"  Composing definition $definition"))
    definition match {
      case Word(word)              => word
      case Keyword("$LINEBREAK")   => newLine
      case Keyword(_)              => ""
      case Reference(rule)         => compose(rule, rules)
      case ComposedWord(words, or) => composeGrouping(words, rules) + " " + composeGrouping(or, rules)
      case g: Grouping             => composeGrouping(g, rules)
    }
  }

  def composeGrouping(grouping: Grouping, rules: Map[Rule, Definition]): String = {
    doInDebug(() => println(s"    Composing grouping $grouping"))
    grouping match {
      case WordGrouping(words)        => getRandom(words).name
      case DefOrGrouping(definitions) => composeDefinition(getRandom(definitions), rules)
      case DefListGrouping(definitions) =>
        doInDebug(() => println(s"    DefListGrouping: {$definitions}"))
        val r = definitions.map(composeDefinition(_, rules)).mkString("")
        doInDebug(() => println("    DefListGrouping result: " + r))
        r
    }
  }

  def getRandom[A](l: List[A]): A = {
    val r = Random.nextInt(l.size)
    val sel = l(r)
    doInDebug(() => println(s"     Random: ${l.size} $r = $sel"))
    sel
  }

  def doInDebug(f: () => Unit) = {
    if (debugEnabled)
      f()
  }
  
  def printDebug(out: String) = {
    if(debugEnabled)
      println(out)
  }

}